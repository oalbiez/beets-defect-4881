FROM python:3.11.4-bullseye

RUN pip install beets

COPY --chmod=0755 run_import /root/
COPY import/ /srv/import/

CMD /root/run_import
