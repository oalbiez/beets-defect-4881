
build:
	DOCKER_BUILDKIT=1 docker build -t beet-bug .

run: build
	docker run -it beet-bug
