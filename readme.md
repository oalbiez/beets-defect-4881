# Bug isolation

This repository was created to reproduce the [bug 4881](https://github.com/beetbox/beets/issues/4881) of beets.


To reproduce the bug, simply do `make run`
